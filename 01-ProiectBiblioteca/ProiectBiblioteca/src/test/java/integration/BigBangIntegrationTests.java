package integration;

import biblioteca.control.BibliotecaCtrl;
import biblioteca.control.BibliotecaCtrlTest;
import biblioteca.model.Carte;
import biblioteca.repository.repo.CartiRepo;
import biblioteca.repository.repo.CartiRepoTest;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.JUnitCore;
import org.junit.runner.Result;
import util.TestUtil;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

/**
 * Created by AlexandruD on 09-May-18.
 */
public class BigBangIntegrationTests {

    private String path = "cartiBDBBint.dat";
    private BibliotecaCtrl ctrl = new BibliotecaCtrl(new CartiRepo(path));

    @Test
    public void runUnit() {
        Result res = JUnitCore.runClasses(BibliotecaCtrlTest.class, CartiRepoTest.class);
        assertTrue("Unit testing step failed. Please run the unit testing phase separately", res.wasSuccessful());
    }

    @Test
    public void bigBangIntegration() throws Exception {

        runUnit();

        // A
        List<String> cuvinteCheie = new ArrayList<String>();
        cuvinteCheie.add("cuvantcheie");
        List<String> referenti = new ArrayList<String>();
        referenti.add("Mihai Eminescu");
        Carte carte = TestUtil.createBook("Luceafarul", "editura", "1993", cuvinteCheie, referenti);
        int oldsize = 0;
        try {
            oldsize = ctrl.getCarti().size();
            ctrl.adaugaCarte(carte);
            assertEquals(ctrl.getCarti().size(), oldsize + 1);
        } catch (Exception e) {
            assert (false);
            e.printStackTrace();
        }

        //B
        final List<Carte> carti;
        try {
            carti = ctrl.cautaCarte("mihai");
            assert (carti.size() == 1);
        } catch (Exception e) {
            assert (false);
            e.printStackTrace();
        }

        //C
        try {
            List<Carte> cartiOrdonateDinAnul = ctrl.getCartiOrdonateDinAnul("1993");
            assert (cartiOrdonateDinAnul.size() == 1);
        } catch (Exception e) {
            assert (false);
        }
    }

    @Before
    public void makeFile() {
        createFile(path);
    }

    @After
    public void clearFile() {
        deleteFile(path);
    }

    private void createFile(String path) {
        File file = new File(path);
        try {
            if (file.createNewFile()) {
                System.out.println("File created");
            }
        }catch(IOException e) {
            e.printStackTrace();
        }
    }

    private void deleteFile(String path) {
        File file = new File(path);
        if(file.delete()) {
            System.out.println("Deletion successfull");
        }
    }

}
