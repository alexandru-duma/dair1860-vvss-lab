package biblioteca.repository.repo;

import biblioteca.model.Carte;
import biblioteca.repository.repoMock.CartiRepoMock;
import org.junit.Before;
import org.junit.Test;

import java.util.List;

import static org.junit.Assert.assertTrue;

/**
 * Created by AlexandruD on 16-Apr-18.
 */
public class CartiRepoTest {

    private CartiRepoMock mock;

    @Test
    public void wbt_cauta_carte_positive() {

        mock.adaugaCarte(Carte.getCarteFromString("a;Dale Carnegie,Emil Cioran;1973;a;b,c"));
        List<Carte> res = mock.cautaCarte("ioran");

        assertTrue("Only one item should be found", res.size() == 1);
        assertTrue("The wrong item has been found", res.get(0).getTitlu().equals("a"));
    }

    @Test
    public void wbt_cauta_carte_negative() {
        List<Carte> res = mock.cautaCarte("a");
        assertTrue("No book should be found", res.size() == 0);
    }

    @Before
    public void clearRepo() {
        mock = new CartiRepoMock();
    }

}
