package biblioteca.control;

import biblioteca.model.Carte;
import biblioteca.repository.repoMock.CartiRepoMock;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.*;

/**
 * Created by AlexandruD on 26-Mar-18.
 */
public class BibliotecaCtrlTest {

    private BibliotecaCtrl ctrl;

    @Test
    public void adaugaCarte_ECP_Positive() throws Exception {
        Carte c = new Carte();
        c.setAnAparitie("123");
        c.setCuvinteCheie(Arrays.asList("a", "b"));
        c.setEditura("ed");
        c.setReferenti(Arrays.asList("a", "b"));
        c.setTitlu("asd");
        ctrl.adaugaCarte(c);

        boolean found = false;

        for(Carte ca: ctrl.getCarti()) {
            if(ca.getTitlu().equals("asd"))
                found = true;
        }

        assertTrue(found);
    }

    @Test(expected = Exception.class)
    public void adaugaCarte_ECP_Negative() throws Exception {
        Carte c = new Carte();
        c.setAnAparitie("123");
        c.setCuvinteCheie(Arrays.asList("a", "b"));
        c.setEditura("ed");
        c.setReferenti(new ArrayList<String>());
        c.setTitlu("asd");
        ctrl.adaugaCarte(c);

        boolean found = false;

        for(Carte ca: ctrl.getCarti()) {
            if(ca.getTitlu().equals("asd"))
                found = true;
        }

        assertTrue(found);
    }

    @Test
    public void adaugaCarte_BVA_Positive() throws Exception {
        Carte c = new Carte();
        c.setAnAparitie("123");
        c.setCuvinteCheie(Arrays.asList("a", "b"));
        c.setEditura("ed");
        c.setReferenti(Arrays.asList("a", "b"));
        c.setTitlu("asd");
        ctrl.adaugaCarte(c);

        boolean found = false;

        for(Carte ca: ctrl.getCarti()) {
            if(ca.getTitlu().equals("asd"))
                found = true;
        }

        assertTrue(found);
    }

    @Test(expected = Exception.class)
    public void adaugaCarte_BVA_Negative() throws Exception {
        Carte c = new Carte();
        c.setAnAparitie("123");
        c.setCuvinteCheie(Arrays.asList("a", "b"));
        c.setEditura("");
        c.setReferenti(Arrays.asList("a", "b"));
        c.setTitlu("asd");
        ctrl.adaugaCarte(c);

        boolean found = false;

        for(Carte ca: ctrl.getCarti()) {
            if(ca.getTitlu().equals("asd"))
                found = true;
        }

        assertTrue(found);
    }

    @Test
    public void test_c() throws Exception {

        ctrl.adaugaCarte(Carte.getCarteFromString("a;b,c;2010;a;c"));
        ctrl.adaugaCarte(Carte.getCarteFromString("a;b,c;2010;a;d"));
        ctrl.adaugaCarte(Carte.getCarteFromString("b;b,c;2010;a;b,c"));
        ctrl.adaugaCarte(Carte.getCarteFromString("b;b,c;1990;a;b,c"));

        List<Carte> res = ctrl.getCartiOrdonateDinAnul("2010");
        for(Carte c: res) {
            assertTrue("A book with a different year was returned", c.getAnAparitie().equals("2010"));
        }

    }

    @Before
    public void setUp() throws Exception {
        ctrl = new BibliotecaCtrl(new CartiRepoMock());
    }

    @After
    public void tearDown() throws Exception {
    }

}
