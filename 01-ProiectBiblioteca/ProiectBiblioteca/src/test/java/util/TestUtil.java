package util;

import biblioteca.model.Carte;

import java.util.List;

/**
 * Created by AlexandruD on 09-May-18.
 */
public class TestUtil {

    public static Carte createBook(String titlu, String editura, String anAparitie, List<String> cuvinteCheie, List<String> referenti) {
        Carte carte = new Carte();
        carte.setTitlu(titlu);
        carte.setAnAparitie(anAparitie);
        carte.setEditura(editura);
        carte.setCuvinteCheie(cuvinteCheie);
        carte.setReferenti(referenti);
        return carte;
    }

}
