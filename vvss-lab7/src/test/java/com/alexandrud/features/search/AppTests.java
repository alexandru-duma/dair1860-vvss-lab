package com.alexandrud.features.search;

import com.alexandrud.steps.serenity.AppUserSteps;
import net.serenitybdd.junit.runners.SerenityRunner;
import net.thucydides.core.annotations.Managed;
import net.thucydides.core.annotations.Steps;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.openqa.selenium.WebDriver;

/**
 * Created by AlexandruD on 16-May-18.
 */
@RunWith(SerenityRunner.class)
public class AppTests {

    @Managed(uniqueSession = true)
    public WebDriver webDriver;

    @Steps
    public AppUserSteps anne;

    @Test
    public void adding_a_valid_element_works() {
        anne.she_stupid_and_visits_our_page();
        anne.submits_something_good();
        anne.should_find("ad a,b c,d 1234 asd");
    }

    @Test
    public void adding_an_invalid_element_doesnt_work() {
        anne.she_stupid_and_visits_our_page();
        anne.submits_something_bad();
        anne.should_see_alert();
    }
}
