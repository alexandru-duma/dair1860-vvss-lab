package com.alexandrud.pages;

import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.core.pages.WebElementFacade;
import net.thucydides.core.annotations.DefaultUrl;
import org.openqa.selenium.support.FindBy;

import java.util.List;
import java.util.function.Predicate;
import java.util.stream.Collectors;

/**
 * Created by AlexandruD on 16-May-18.
 */
@DefaultUrl("http://localhost:1234/home")
public class AppPage extends PageObject {

    @FindBy(id = "submit-button")
    private WebElementFacade submitButton;

    @FindBy(id="titlu")
    private WebElementFacade titleInput;

    @FindBy(id="referenti")
    private WebElementFacade authorsInput;

    @FindBy(id="cuvinteCheie")
    private WebElementFacade keywordInput;

    @FindBy(id="anAparitie")
    private WebElementFacade yearInput;

    @FindBy(id="editura")
    private WebElementFacade publisherInput;

    public List<String> getData() {
        return findAll(".data").stream()
                .map(e -> e.getText())
                .collect(Collectors.toList());
    }

    public boolean checkData(Predicate<String> pred) {
        return getData().stream().anyMatch(pred);
    }


    private void submit(String title, String authors, String keywords, String year, String published) {

        titleInput.clear();
        titleInput.type(title);

        authorsInput.clear();
        authorsInput.type(authors);

        keywordInput.clear();
        keywordInput.type(keywords);

        yearInput.clear();
        yearInput.type(year);

        publisherInput.clear();
        publisherInput.type(published);

        submitButton.click();
    }

    public boolean alertPresent() {
        return getAlert() != null;
    }

    public void submitNegative() {
        submit("ad", "a,b", "c,d", "a", "asd");
    }

    public void submitPositive() {
        submit("ad", "a,b", "c,d", "1234", "asd");
    }

}
