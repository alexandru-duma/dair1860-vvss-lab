package com.alexandrud.steps.serenity;

import com.alexandrud.pages.AppPage;
import net.thucydides.core.annotations.Step;

/**
 * Created by AlexandruD on 16-May-18.
 */
public class AppUserSteps {

    AppPage appPage;

    @Step
    public void submits_something_good() {
        appPage.submitPositive();
    }

    @Step
    public void submits_something_bad() {
        appPage.submitNegative();
    }

    @Step
    public void she_stupid_and_visits_our_page() {
        appPage.open();
    }

    @Step
    public void should_find(String line) {
        assert(appPage.checkData(e -> e.equals(line)));
    }

    @Step
    public void should_see_alert() {
        assert(appPage.alertPresent());
    }
}
